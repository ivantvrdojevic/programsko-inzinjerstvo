from query_handler_base import QueryHandlerBase
import requests

class VaccovidHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "covid" in query:
            return True
        return False

    def process(self, query):
        names = query.split()
        name = names[1]
        short = names[2]

        try:
            result = self.call_api(name, short)
            self.ui.say(f"Showing data for {name}, {short}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact vaccovid api.")
            self.ui.say("Try something else!")

    def call_api(self, name, short):

        querystring = {"name":name,"short":short}

        url = (f"https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/country-report-iso-based/{name}/{short}")

        headers = {
	'X-RapidAPI-Key': '534f4922b6mshd310b4b8699ded4p1efd2ajsn8cde7a03e98c',
    'X-RapidAPI-Host': 'vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com'
}

        response = requests.request("GET", url, headers=headers)

        print(response.text)

