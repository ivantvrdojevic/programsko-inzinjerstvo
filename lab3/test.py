import requests

url = "https://love-calculator.p.rapidapi.com/getPercentage"

querystring = {"sname":"Alice","fname":"John"}

headers = {
	"X-RapidAPI-Key": "6a1b03319amsh2bd088ee43e0218p1cc929jsna512f87ff0b2",
	"X-RapidAPI-Host": "love-calculator.p.rapidapi.com"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)